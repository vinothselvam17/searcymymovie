/*Search Movie Component*/
/* This is the place where all the search for your movie happens*/

import React, { useState } from 'react';

const Search = ({search}) => {

	const [ searchValue, setSearchValue ] = useState('');

	const resetInputField = () => {
		setSearchValue('');
	}

	const callMovieSearch = (e) => {
		e.preventDefault();
		resetInputField();
	}

	const handleSearchInputChanges = (e) => {
		setSearchValue(e.target.value);
		search(e.target.value);
	}

	return (
		<div>
			<form className="search">
				<input value={searchValue} onChange={handleSearchInputChanges} type="text"/>
				<button type="button" class="btn btn-primary" onClick={callMovieSearch}><span class="glyphicon glyphicon-search"></span>Reset</button>
			</form>
		</div>
	);
};

export default Search;