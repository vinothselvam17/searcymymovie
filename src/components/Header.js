/*Header Component*/

import React from 'react';
import logo from '../assets/movie.jpg';

const Header = (props) => {

	return (
		<nav class="navbar navbar-expand-sm navbar-dark bg-primary">
			    <a class="navbar-brand" href="#">
		          <img src={logo} alt="Movie Logo" style={{width:"50px"}}/>
		        </a>
				<header className="App-header">
					<h2>{props.text}</h2>
				</header>
		</nav>
	);
};

export default Header;