import React, { useEffect, useReducer } from 'react';
import spinner from './assets/loader.gif';
import { initialState, reducer } from './store/reducer/index.js'
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Search from './components/Search';
import Movie from './components/Movie';
import axios from 'axios';

const MOVIE_API_URL = "https://www.omdbapi.com/?s=man&apikey=4a3b711b";
//https://www.omdbapi.com/?s=man&apikey=4a3b711b
//http://www.omdbapi.com/?i=tt3896198&apikey=c87ba20b


const App = () => {
  /* Invoke reducer function for a new state based on the action specified */
  const [ state, dispatch ] = useReducer(reducer, initialState);

  /* On time load API calls */
  useEffect( () => {

    axios.get(MOVIE_API_URL)
         .then( response => {
            response.data.Response === "True" ? dispatch({
              type: 'SEARCH_MOVIE_SUCCESS',
              payload: (response.data.Search).sort( (a,b) => sortByMultipleProperties(a,b) )
            }): dispatch({
              type: 'SEARCH_MOVIE_FAILURE',
              error: response.data.Error
            })
          })
         .catch( err => {
            dispatch({
              type: 'SEARCH_MOVIE_FAILURE',
              error: err
            })
          });
  },[]);
  //If no array, then it indefintely calls
  //If an array with emoty values, calls one time
  //If an array specified with values, calls every time when there is a value change

  const sortByMultipleProperties = (a, b) => {
    //Sort by Year descending and Title Ascending
    if ( b.Year === a.Year )
         return (a.Title > b.Title) ? 1 : -1;
    return (b.Year > a.Year) ? 1 : -1;
  }

    /*Higher Order function which takes function as an input and outputs*/
  const debounce = (fn, time) => {
    let timeout;
    return function(){

      const functionCall = () => fn.apply(this,arguments);
      /*first time call it  clears nothing*/
      clearTimeout(timeout);
      timeout = setTimeout(functionCall, time); //Function had to be invoked with a delay of specified timeout

    }
  }

  /*Propogating the search API call from child to Parent*/
  const search = debounce( (searchValue) => {

      dispatch({
        type: 'SEARCH_MOVIE_REQUEST'        
      })

      axios.get(`https://www.omdbapi.com/?s=${searchValue}&apikey=4a3b711b`) //https://www.omdbapi.com/?s=${searchValue}&apikey=4a3b711b //http://www.omdbapi.com/?i=${searchValue}&apikey=c87ba20b
           .then( response => {
            response.data.Response === "True" ? dispatch({
              type: 'SEARCH_MOVIE_SUCCESS',
              payload: (response.data.Search).sort( (a,b) => sortByMultipleProperties(a,b) )
            }): dispatch({
              type: 'SEARCH_MOVIE_FAILURE',
              error: response.data.Error
            })
          })
         .catch( err => {
            dispatch({
              type: 'SEARCH_MOVIE_FAILURE',
              error: err
            })
          });

  }, 2000 );

  const { movies, errorMessage, loading } = state;


  const retrievedMovies = loading && !errorMessage ? 
                          <img className="spinner" src={spinner} alt="Loading spinner"/> : errorMessage ? 
                          <div className="errorMessage">{errorMessage}</div> : 
                           (movies.map( (movie, index) => {
                              return <Movie key={`${index}-${movie.Title}`} movie={movie}/>
                           }));

  return (
    <div className="App">
      <Header text="Search My Movie" logo={logo}/>
      <Search search={search}/>
      <div className="movies">{retrievedMovies}</div>
    </div>
  )
}

export default App;
